import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import br.com.caelum.JpaConfigurator;

public class TestaPool {

	public static void main(String[] args) throws PropertyVetoException, SQLException {

		ComboPooledDataSource dataSoucer = (ComboPooledDataSource) new JpaConfigurator().getDataSource();

		for (int i = 0; i < 10; i++) {

			Connection connection = dataSoucer.getConnection();

			System.out.println("Conexoes ocupadas: " + dataSoucer.getNumBusyConnections());
			System.out.println("Conexoes livres: " + dataSoucer.getNumIdleConnections());
			System.out.println(); 
		}
	}

}
